# COACT #



### What is COACT? ###

**COACT** (**C**omprehensive **O**ffice **A**utomation & **C**ollaboration **T**oolkit) is a collection of AI-powered **RPA** (**R**obotic **P**rocess **A**utomation) bots that automate front-office tasks.